# STAT4984_Final_Project

When trying to classify images, typical neural networks have a hard time scaling well to full images. This is where Convolutional Neural Networks can be of use. They constrain the architecture of the neural network in a way that takes advantage of the fact that the input consists of images. In this project I will attempt to build a CNN, specifically following VGGNET architecture to classify CIFAR-10 images. 

Included in the repository are the files used to create VGGNET network and a short notebook to visualize CIFAR10 data. 
